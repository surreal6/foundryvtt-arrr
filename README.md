# ¡Arrr! · Sistema para Foundry VTT

Esta es una adaptación no oficial para jugar a ¡Arrr! en Foundry VTT.

Por el momento es una adaptación muy sencilla que no dispone de automatismos.

En el podrás encontrar fichas de personaje para Aventureros, para PNJs y para barcos.
Incluye también compendios con los talentos disponibles,una recopilación de las armas, armaduras y hechizos que figuran en el libro.

En un futuro tengo intención de ir añadiendo resúmenes de las reglas y algunas de las tablas necesarias para jugar.

## Créditos del juego original:

#¡ARRR! · VIEJA ESCUELA · PIRATAS

Escrito por Falenthal

## Basado en Vieja Escuela: el juego de rol
escrito por Javier “cabohicks” García

## Con ideas y reglas aportadas por
Eneko Palencia, Eneko Menica, David F. Fernández,
Capitán Mordigan, Maurick Starkvind,
Ramón Balcells (VE Cyberpunk)
y Manu Sáez (VE Salvaje Oeste)

## Pruebas de juego
Calfred, César Bernal, Joan Farré, Salomé Sánchez Bernal,
Kapithan, Iosu Larumbe, Capitán Mordigan, David F. Fernández,
Ivanmarte, Eneko Palencia y Club de Rol Condado del Deza

## Edición y corrección
Eneko Palencia

## Mapa de Jamaica por
Eneko Menica

## Diseño y maquetación
Eneko Palencia

## grapas&mapas
v1.1 - Octubre de 2020
http://grapasymapas.com

#Licencia
Creative Commons Reconocimiento – NoComercial – CompartirIgual
(https://creativecommons.org/licenses/by-nc-sa/4.0/)
